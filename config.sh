#!/bin/bash
cp -a . /opt/quiubo-kernel
cd /opt/quiubo-kernel
workpath=$PWD
echo "${workpath}"
mainfilepath=/main.py
workpath+=$mainfilepath
echo "${workpath}"
chmod +x "${workpath}"
touch /usr/share/applications/quiubo-kernel.desktop

desktopshortcut="[Desktop Entry]\nVersion=0.1.2\nName=Quiubo Kernel\nExec=python3 /opt/quiubo-kernel/main.py\nPath=/opt/quiubo-kernel/\nTerminal=false\nType=Application\nCategories=Utility;Application;"

echo -e $desktopshortcut >> /usr/share/applications/quiubo-kernel.desktop

