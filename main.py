import re
import subprocess
import gi
from threading import Thread
import logging
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib

class Handler:

    def __init__(self):
        super().__init__()
        
        kernel_a_actuar = builder.get_object("kernel_a_actuar")
        kernel_a_actuar.set_radio(True)
        kernel_a_actuar.connect("toggled", self.on_cell_toggled)
        kernel_a_actuar1 = builder.get_object("kernel_a_actuar1")
        kernel_a_actuar1.set_radio(True)
        kernel_a_actuar1.connect("toggled", self.on_cell_toggled1)

    def on_tree_selection_changed(self, selection):
        model, treeiter = selection.get_selected()
        if treeiter != None:
            print ("You selected", model[treeiter][0])

    def onDestroy(self, *args):
        Gtk.main_quit()
    
    def spin(self):
        procesando.start()
        return False

    def on_cell_toggled(self, widget, path):
        selected_path = Gtk.TreePath(path)
        for row in lista_kernels:
            row[1] = row.path == selected_path
    
    def on_cell_toggled1(self, widget, path):
        selected_path = Gtk.TreePath(path)
        for row in lista_kernels1:
            row[1] = row.path == selected_path

    def list_commands_in_another_thread(self):
        GLib.idle_add(self.spin)
        try:
            list = subprocess.run(["kernel-criollo"], stdout=subprocess.PIPE)
            list = str(list.stdout.decode('utf-8'))
            stdout_act.set_text(str("Completado"))
        except subprocess.CalledProcessError as e:
            stdout_act.set_text(str("Error!"))
            logging.basicConfig(filename='quibo-kernel.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')
            logging.warning(e)
        for l in list.splitlines():
            res =  re.search(r"^\s.*", l)
            if res:
                lista_kernels.append([str(res.group(0)), False])
        procesando.stop()

    def list_commands(self, button):
        GLib.idle_add(self.spin)
        lista_kernels.clear()
        lista_kernels1.clear()
        #list = subprocess.run(["kernel-criollo"], stdout=subprocess.PIPE)
        #list = str(list.stdout.decode('utf-8'))
        thread = Thread(target=self.list_commands_in_another_thread)
        thread.daemon = True
        thread.start()
        #procesando.stop()

    def list_packages_in_another_thread(self):
        GLib.idle_add(self.spin)
        try:
            lista = subprocess.run(['pkexec','--disable-internal-agent', "kernel-criollo", "list"], stdout=subprocess.PIPE)
            lista = str(lista.stdout.decode('utf-8'))
            kernels_disponibles = []
            kernels_instalados = []
            temp_vari = []
            total_stdout = lista.splitlines()
            for l in lista.splitlines():
                res =  re.search(r"^[0-9].*", l)
                instalado =  re.search(r"^Current.*", l)
                if res:
                    kernels_disponibles.append(res.group(0))
                if instalado:
                    break
                temp_vari.append(l)
            i = len(temp_vari)
            new_list = total_stdout[i:]
            for l in new_list:
                res =  re.search(r"^[0-9].*", l)
                if res:
                    kernels_instalados.append(res.group(0))
            for kernel in kernels_disponibles:
                lista_kernels.append([str(kernel), False])
            for kernel in kernels_instalados:
                lista_kernels1.append([str(kernel), False])
        except subprocess.CalledProcessError as e:
            stdout_act.set_text(str("Error al listar!"))
            logging.basicConfig(filename='quibo-kernel.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')
            logging.warning(e)
        procesando.stop()

    def list_packages(self, button):
        GLib.idle_add(self.spin)
        stdout_act.set_text(str(" "))
        lista_kernels.clear()
        lista_kernels1.clear()
        thread = Thread(target=self.list_packages_in_another_thread)
        thread.daemon = True
        thread.start()
        procesando.stop()
        return True
    
    def install_in_another_thread(self):
        GLib.idle_add(self.spin)
        try:
            for path in self.pathlist :
                tree_iters = self.model.get_iter(path)
                krnel_to_install = self.model.get_value(tree_iters,0)
                print(krnel_to_install)
            list = subprocess.run(['pkexec','--disable-internal-agent', "kernel-criollo", "install", str(krnel_to_install)], stdout=subprocess.PIPE)
            print(list.stdout.decode('utf-8'))
            stdout_act.set_text("Kernel instalado con exito")
        except subprocess.CalledProcessError as e:
            stdout_act.set_text(str("Error al instalar!"))
            logging.basicConfig(filename='quibo-kernel.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')
            logging.warning(e)
        procesando.stop()
    
    def install(self, button):
        GLib.idle_add(self.spin)
        stdout_act.set_text(str(" "))
        self.to_install_pkg = lista_kernels_a_instalar.get_selection()
        self.to_install_pkg.connect('changed', self.on_tree_selection_changed)
        (self.model, self.pathlist) =  self.to_install_pkg.get_selected_rows()
        thread = Thread(target=self.install_in_another_thread)
        thread.daemon = True
        thread.start()
        procesando.stop()
        lista_kernels1.clear()
        lista_kernels.clear()
        return False

    def remove_in_another_kernel(self):
        try:
            for path in self.pathlist :
                tree_iters = self.model.get_iter(path)
                krnel_to_remove = self.model.get_value(tree_iters,0)
                print("kernel a remover", krnel_to_remove)
            list = subprocess.run(['pkexec','--disable-internal-agent', "kernel-criollo", "remove", str(krnel_to_remove)], stdout=subprocess.PIPE)
            stdout_act.set_text("Kernel removido con exito")
        except subprocess.CalledProcessError as e:
            stdout_act.set_text(str("Error al remover!"))
            logging.basicConfig(filename='quibo-kernel.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')
            logging.warning(e)
        procesando.stop()

    def remove(self, button):
        GLib.idle_add(self.spin)
        stdout_act.set_text(str(" "))
        self.to_remove_pkg = lista_kernels_a_remover.get_selection()
        self.to_remove_pkg.connect('changed', self.on_tree_selection_changed)
        (self.model, self.pathlist) =  self.to_remove_pkg.get_selected_rows()
        thread = Thread(target=self.remove_in_another_kernel)
        thread.daemon = True
        thread.start()
        procesando.stop()
        lista_kernels1.clear()
        lista_kernels.clear()
        return False

builder = Gtk.Builder()
builder.add_from_file("templates/Quiubo Kernel.glade")
builder.connect_signals(Handler())

window = builder.get_object("window")

lista_kernels = builder.get_object("lista_kernels")
lista_kernels1 = builder.get_object("lista_kernels1")

kernel_a_actuar = builder.get_object("kernel_a_actuar")
kernel_a_actuar1 = builder.get_object("kernel_a_actuar1")
lista_kernels_a_instalar = builder.get_object("lista_kernels_disponibles")
lista_kernels_a_remover = builder.get_object("lista_kernels_instalados")
stdout_act = builder.get_object("stdout")
stderr_act = builder.get_object("stderr")
procesando = builder.get_object("procesando")

window.show_all()

Gtk.main()
